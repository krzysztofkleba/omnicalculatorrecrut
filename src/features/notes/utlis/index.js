
export const LOCAL_STORAGE_NOTES_KEY = 'persistedNotes';

/**
 * Returns persisted notes or undefined when any found.
 * @returns {Array} Returns persisted notes list or undefined when any found.
 */
export const fetchPersistedNotes = () => {
    const persistedNotes = JSON.parse(window.localStorage.getItem(LOCAL_STORAGE_NOTES_KEY));
    if (persistedNotes !== null) {
        return JSON.parse(persistedNotes);
    }
    return undefined;
}

/**
 * Persist notes in local storage if needed.
 * @param {Object} prevState Previous state object.
 * @param {Object} currentNotesState Current state object.
 */
export const persistNotes = (prevState, currentState) => {
    const prevNotesState = JSON.stringify(prevState);
    const currentNotesState = JSON.stringify(currentState);

    if (prevNotesState !== currentNotesState) {
        window.localStorage.setItem(LOCAL_STORAGE_NOTES_KEY, JSON.stringify(currentNotesState));
    }
}