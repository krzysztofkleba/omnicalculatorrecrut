import React from 'react';
import PropTypes from 'prop-types';
import ReactMarkdown from 'react-markdown';
import { Link } from 'react-router-dom';
import { LABELS_DELETE_NOTE } from '../../../core/labels';
import '../NotesList.css';

const SingleNote = props => {
    const { id, text, createdAt } = props.note;
    const dateOptions = { day: '2-digit', month: '2-digit', year: 'numeric' };
    return (
        <div className="SingleNote-root">
            <div className="SingleNote-text-container">
                <div className="SingleNote-text">
                    <ReactMarkdown>
                        {text}
                    </ReactMarkdown>
                </div>
                <div className="SingleNote-date">
                    <Link to={`/${id}`}>{new Date(createdAt).toLocaleDateString('en-US', dateOptions)}</Link>

                </div>
            </div>
            <div className="SingleNote-buttons">
                <button
                    className="btn-danger"
                    onClick={() => props.deleteNote(id)}
                >
                    {LABELS_DELETE_NOTE}
                </button>
            </div>
        </div >
    );
};

SingleNote.propTypes = {
    deleteNote: PropTypes.func.isRequired
};

export default SingleNote;