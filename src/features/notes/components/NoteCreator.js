import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as Labels from '../../../core/labels';

const NoteCreator = props => {
    const [text, setText] = useState('');

    const addNote = text => {
        const createdAt = Date.now();
        props.addNote({
            id: createdAt.toString(),
            text,
            createdAt
        });
        setText('');
    };

    return (
        <div className="NoteCreator-root">
            <div className="NoteCreator-title">
                <h2>{Labels.LABEL_NOTE}</h2>
            </div>
            <div className="NoteCreator-text">
                <textarea
                    id="NoteCreator-textarea"
                    name="NoteCreator-textarea"
                    rows="10"
                    cols="33"
                    maxLength="255"
                    placeholder={Labels.LABEL_NOTE_TEXT_PLACEHOLDER}
                    onChange={event => setText(event.target.value)}
                    value={text}
                >
                </textarea>
            </div>
            <div className="NoteCreator-buttons">
                <button
                    className="btn-light"
                    onClick={() => addNote(text)}>
                    {Labels.LABEL_ADD_NOTE}
                </button>
            </div>
        </div>
    );
};

NoteCreator.propTypes = {
    addNote: PropTypes.func.isRequired
};

export default NoteCreator;