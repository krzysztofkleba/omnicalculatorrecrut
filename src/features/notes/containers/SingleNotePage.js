import React, { useEffect, useState } from 'react';
import ReactMarkdown from 'react-markdown';
import { useHistory, useParams } from 'react-router';
import { LABELS_DELETE_NOTE, LABEL_GO_BACK, ERROR_NOTE_URL_NOT_FOUND } from '../../../core/labels';
import { fetchPersistedNotes } from '../utlis';
import '../NotesList.css';

const SingleNotePage = props => {
    const history = useHistory();
    const { id } = useParams();
    const [note, setNote] = useState();
    const dateOptions = { day: '2-digit', month: '2-digit', year: 'numeric' };

    useEffect(() => {
        const currentNote = props.notes.find(entry => entry.id === id);
        if (currentNote) {
            setNote(currentNote);
        } else {
            const persistedNotes = fetchPersistedNotes();
            if (persistedNotes) {
                const persistedNote = persistedNotes.find(entry => entry.id === id);
                setNote(persistedNote);
            }
        }
    }, [props.notes]);

    const deleteNote = noteId => {
        history.push('/', {
            deleteAction: {
                noteId: noteId,
                finished: false
            }
        });
    };

    const goBackButton = (
        <button
            className="btn-light"
            onClick={() => history.push('/')}
        >
            {LABEL_GO_BACK}
        </button>
    );

    if (!note) {
        return (
            <div className="NotesContainer">
                <div className="SingleNotePage-buttons">
                    {goBackButton}
                </div>
                {ERROR_NOTE_URL_NOT_FOUND}
            </div>
        );
    }

    return (
        <div className="NotesContainer">
            <div className="SingleNotePage-buttons">
                {goBackButton}
                <button
                    className="btn-danger"
                    onClick={() => deleteNote(id)}
                >
                    {LABELS_DELETE_NOTE}
                </button>
            </div>
            <div className="SingleNotePage-root">
                <div className="SingleNotePage-text-container">
                    <div className="SingleNotePage-text">
                        <ReactMarkdown>
                            {note.text}
                        </ReactMarkdown>
                    </div>
                    <div className="SingleNotePage-date">{new Date(note.createdAt).toLocaleDateString('en-US', dateOptions)}</div>
                </div>
            </div>
        </div >
    );
};

export default SingleNotePage;