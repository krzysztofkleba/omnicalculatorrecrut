import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import PropTypes from 'prop-types';
import { LABEL_LATEST_NOTES } from '../../../core/labels';
import NoteCreator from '../components/NoteCreator';
import SingleNote from '../components/SingleNote';
import { fetchPersistedNotes } from '../utlis';
import '../NotesList.css';

const NotesListPage = props => {
    const history = useHistory();
    const { updateNotes } = props;
    const [notes, setNotes] = useState(props.notes);

    useEffect(() => {
        const persistedNotes = fetchPersistedNotes();
        if (persistedNotes) {
            setNotes(persistedNotes);
        }
    }, []);

    useEffect(() => {
        updateNotes(notes);
    }, [notes, updateNotes]);

    useEffect(() => {
        // i could use rxJs.. but why not use react-router for "experimental use" :D 
        const deleteAction = history.location?.state?.deleteAction;
        if (deleteAction && deleteAction?.finished === false) {
            deleteNote(deleteAction.noteId);
            history.replace('/', {});
        }
    }, [history.location]);

    const addNote = note => {
        setNotes([note].concat(notes))
    };

    const deleteNote = noteId => {
        const filteredNotes = notes.filter(note => note.id !== noteId);
        setNotes(filteredNotes);
    };

    return (
        <div className="NotesContainer">
            <NoteCreator addNote={addNote} />
            <div className="NotesListPage-container">
                <div className="NotesListPage-header">
                    <span>{LABEL_LATEST_NOTES}</span>
                </div>
                <div className="NotesListPage-noteList">
                    {notes.map(note => <SingleNote
                        key={`note-${note.id}`}
                        note={note}
                        deleteNote={deleteNote}
                    />)}
                </div>
            </div>
        </div>
    );
};

NotesListPage.propTypes = {
    notes: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string,
        text: PropTypes.string,
        createdAt: PropTypes.number,
    })).isRequired,
    updateNotes: PropTypes.func.isRequired
};


export default NotesListPage;