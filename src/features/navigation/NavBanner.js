import React from 'react';
import { LABEL_APP_NAME } from '../../core/labels';
import './NavBanner.css'

const NavBanner = () => {
    return (
        <div className="AppBanner">
            <div className="AppBanner-logo">{LABEL_APP_NAME}</div>
        </div>
    );
};

export default NavBanner;