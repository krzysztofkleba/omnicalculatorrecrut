// general
export const LABEL_APP_NAME = 'Notes App';
export const LABEL_GO_BACK = 'Go back';

// Notes
export const LABEL_NOTE = 'Note';
export const LABEL_ADD_NOTE = 'Add note';
export const LABELS_DELETE_NOTE = 'Delete note';
export const LABEL_NOTE_TEXT_PLACEHOLDER = "Note text";
export const LABEL_LATEST_NOTES = 'Latest Notes';

// Errors
export const ERROR_NOTE_URL_NOT_FOUND = 'Any note match corresponding url!';