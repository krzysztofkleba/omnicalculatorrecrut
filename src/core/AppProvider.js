import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import NavBanner from '../features/navigation/NavBanner';
import NotesListPage from '../features/notes/containers/NotesListPage';
import SingleNotePage from '../features/notes/containers/SingleNotePage';
import { persistNotes } from '../features/notes/utlis';
import './AppProvider.css';

class AppProvider extends Component {

    constructor (props) {
        super(props);
        this.state = {
            notes: [
                {
                    id: '1629316698069',
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer tincidunt at massa non scelerisque. Vestibulum feugiat quam at augue varius placerat. Praesent pellentesque maximus ex sed ullamcorper. Mauris auctor, elit auctor mollis tempus, arcu mi sagittis nibh, quis condimentum massa massa ornare augue. Nam commodo fermentum convallis. Pellentesque diam eros, vulputate sit amet lacus vitae, rutrum pellentesque est. Fusce semper lacus in mi venenatis vulputate. Praesent eu orci luctus, pretium nisl quis, vulputate dui. Maecenas rutrum odio id mollis vehicula.',
                    createdAt: 1629316698069
                }
            ]
        };
    }

    componentDidUpdate (prevProps, prevState) {
        persistNotes(prevState.notes, this.state.notes);
    }

    render () {
        return (
            <Router>
                <NavBanner />
                <div className="AppContainer-root">
                    <Switch>
                        <Route path="/:id">
                            <SingleNotePage notes={this.state.notes} />
                        </Route>
                        <Route path="/">
                            <NotesListPage
                                notes={this.state.notes}
                                updateNotes={this._updateNotes}
                            />
                        </Route>
                    </Switch>
                </div>
            </Router>
        );
    }

    //#region private methods

    _updateNotes = notes => this.setState({ notes });

    //#endregion
}
export default AppProvider;